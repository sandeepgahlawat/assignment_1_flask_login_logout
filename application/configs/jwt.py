import os

class JWTConfig:
    JWT_SECRET = os.environ.get("JWT_SECRET", "jwt_secret_key")
