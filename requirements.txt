Flask==1.1.4
Flask-SQLAlchemy==2.4.1
Flask-Cors==3.0.8
Flask-Migrate==2.5.2
PyJWT==1.7.1
mysqlclient==2.1.1
SQLAlchemy==1.3.24
MarkupSafe==2.0.1
